# WebSocket

HTTP Upgrade for full-duplex communication. https://en.m.wikipedia.org/wiki/WebSocket

* [Can I use _?](https://caniuse.com/#search=WebSocket)

# Keywords for searching about
* [websocket vs](https://google.com/search?q=websocket+vs)
* [websocket vs webrtc](https://google.com/search?q=websocket+vs+webrtc)

# Libraries for web-sites back-ends
## uWSGI
* [*WebSocket support*](https://uwsgi-docs.readthedocs.io/en/latest/WebSockets.html)

## Elixir
* [*Elixir WebSocket Chat Example w/o Phoenix*
  ](https://medium.com/@loganbbres/elixir-websocket-chat-example-c72986ab5778)
  2018-12 Logan Bresnahan

## Haskell

## Node.js
* [*Node.js & WebSocket — Simple chat tutorial*
  ](https://medium.com/@martin.sikora/node-js-websocket-simple-chat-tutorial-2def3a841b61)
  2017-05 Martin Sikora

## Ocaml

## Perl
### Mojolicious

## PHP
* [*PHP & Websocket: are we ready yet?*
  ](https://tsh.io/blog/php-websocket/)
  2019-02 Adrian Chlubek
* [*How to create websockets server in PHP*
  ](https://stackoverflow.com/questions/14512182/how-to-create-websockets-server-in-php)
* https://phppackages.org/s/WebSocket
* cboden/ratchet
...
* bloatless/php-websocket

## Python
### Sans-I/O: wsproto
* [*Getting Started*](https://wsproto.readthedocs.io/en/latest/basic-usage.html)
* https://tracker.debian.org/pkg/python-wsproto

### websockets
* https://pypi.org/project/websockets

## Raku
### Cro
* https://cro.services/docs

## Rust
...
### wrap
* https://crates.io/crates/warp

## Swift